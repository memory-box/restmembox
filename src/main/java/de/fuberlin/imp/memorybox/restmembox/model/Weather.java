package de.fuberlin.imp.memorybox.restmembox.model;

public enum Weather {
    RAIN, CLOUD, SNOW, STORM, THUNDER, CLEAR
}
