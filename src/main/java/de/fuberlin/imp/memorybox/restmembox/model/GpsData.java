package de.fuberlin.imp.memorybox.restmembox.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Embeddable
public class GpsData {

    @Column(name="gps_longitude")
    private float longitude;
    @Column(name="gps_latitude")
    private float latitude;
    @Column(name="gps_altitude")
    private float altitude;

}
