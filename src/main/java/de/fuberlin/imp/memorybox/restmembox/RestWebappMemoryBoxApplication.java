package de.fuberlin.imp.memorybox.restmembox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class RestWebappMemoryBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestWebappMemoryBoxApplication.class, args);
	}
}
