package de.fuberlin.imp.memorybox.restmembox.model;

/**
 * Possible tones returned from the Watson API.
 */
public enum Tone {
    FRUSTRATION, IMPOLITENESS, SADNESS, SYMPATHY, POLITENESS,
    SATISFACTION, EXCITEMENT
}
