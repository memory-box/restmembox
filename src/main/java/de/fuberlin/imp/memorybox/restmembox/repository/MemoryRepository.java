package de.fuberlin.imp.memorybox.restmembox.repository;

import de.fuberlin.imp.memorybox.restmembox.model.Memory;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface MemoryRepository extends PagingAndSortingRepository<Memory, Long> {

}
