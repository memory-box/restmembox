package de.fuberlin.imp.memorybox.restmembox.model;

import lombok.Data;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.awt.*;
import java.time.LocalDateTime;

@Data
@Entity
public class Memory {

    @Id
    @GeneratedValue()
    private long id;
    private final LocalDateTime creationDate;
    private boolean published;

    // user?

    @Embedded
    private GpsData gpsData;

    private byte[] voice;
    private String voiceText;
    private Tone tone;
    private byte[] image;
    private String note; // optional Text
    private Weather weather;

    // memstone
    private byte[] mesh;
    private Color color;

    public Memory() {
        this.creationDate = LocalDateTime.now();
        this.published = false;
    }
}
