FROM openjdk:8-jre-alpine
COPY target/restmembox-0.2-SNAPSHOT.jar /opt/memorybox/app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom",  "-Dspring.profiles.active=docker", "-jar", "/opt/memorybox/app.jar"]
EXPOSE 8080